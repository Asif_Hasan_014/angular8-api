export class ApiResponse{
    timestamp:string;
    status:string;
    message:string;
    totalCount:number;
    numberOfElements:number;
    content:any[];
}