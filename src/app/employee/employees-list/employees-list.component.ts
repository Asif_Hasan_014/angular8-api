import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable  } from 'rxjs';
import { ApiResponse } from 'src/app/model/api.response';
import { Employee } from 'src/app/model/employee.model';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {
  errMeg:string;
  value:any;
  employees;
  // employees= [
  //       {
  //           "id": 1,
  //           "name": "Asif Hasan",
  //           "email": "asif@gmail.com",
  //           "phone": "01863622375",
  //           "description": "He is a good boy"
  //       },
  //       {
  //           "id": 2,
  //           "name": "Adib Hasan",
  //           "email": "adib@gmail.com",
  //           "phone": "01963622375",
  //           "description": "He is a bad boy"
  //       }
  //   ];
 
  constructor(private employeeService : EmployeeService, private router:Router) { 
    // console.log("employees "+this.employees.content[0].name);
    // console.log(this.employees);
  }

  ngOnInit() {
    this.employeeService.getEmployees().subscribe((data)=>{
      console.log("data value");
      console.log(data);
      console.log("data message");
      console.log(data.message);
      // this.employees = data['content'];
      this.employees = data.content;
      console.log("employees");
      console.log(this.employees);

    });
  }

  deleteEmployee(id:number){
    this.employeeService.deleteEmployee(id).subscribe((data)=>{
      console.log("data value");
      console.log(data);
      this.value=data;
    }, error => console.log(error));
    var r=confirm(this.value.message);
    if (r == true) {
      location.reload();
    } 
  }

  updateEmployee(id:number){
     this.router.navigate(["update",id]);
  }

}
