import { Injectable } from '@angular/core';
import {from, Observable} from 'rxjs';
import { ApiResponse } from '../model/api.response';
import {HttpClient} from '@angular/common/http'
import { Employee } from '../model/employee.model'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = "http://localhost:8080/api/employee";
  private deleteOrUpdateBaseUrl: string = "http://localhost:8080/api/employee/";
  // environment.baseUrl+'/api/employees/';

  

  public getEmployees():any{
    return this.http.get<any>(this.baseUrl);
  }

  getEmployeeById(id: number): any {
    return this.http.get<any>(this.deleteOrUpdateBaseUrl + id);
  }

  createEmployee(employee: Employee): any {
    return this.http.post<any>(this.baseUrl, employee);
  }

  updateEmployee(id: number, employee: Employee): any {
    return this.http.put<any>(this.deleteOrUpdateBaseUrl + employee.id, employee);
  }

  deleteEmployee(id: number): any {
    return this.http.delete<any>(this.deleteOrUpdateBaseUrl + id);
  }
}
